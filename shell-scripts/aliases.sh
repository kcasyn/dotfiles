## General Aliases
alias em='emacsclient -s ~/.emacs.d/server/server'
alias pyhttpd='python -m SimpleHTTPServer 8080'
alias pyftpd='python -m pyftpdlib'

## Docker Aliases
# place aliases for docker containers here
