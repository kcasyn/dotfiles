cd $HOME && \
      sudo apt-get install -y zsh && \
      sh -c "$(wget -O- https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" && \
      git clone https://github.com/denysdovhan/spaceship-prompt.git "$ZSH_CUSTOM/themes/spaceship-prompt" && \
      ln -s "$ZSH_CUSTOM/themes/spaceship-prompt/spaceship.zsh-theme" "$ZSH_CUSTOM/themes/spaceship.zsh-theme" && \
      git clone https://github.com/zsh-users/zsh-autosuggestions "$ZSH_CUSTOM/plugins/zsh-autosuggestions" && \
      sed -i 's/ZSH_THEME="robbyrussell"/ZSH_THEME="spaceship"/' .zshrc && \
      sed -i 's/plugins=(git)/plugins=(git debian web-search zsh-autosuggestions)/' .zshrc && \
      cat <<EOF >> .zshrc
## Settings
RPROMPT="[%@ ]"
export TMUX=
unset TMPDIR

## Sources
source "/home/kcasyn/.dotfiles/functions.sh"
source "/home/kcasyn/.dotfiles/aliases.sh"
EOF

cd $HOME/.oh-my-zsh/custom/themes/ && \
    sed -i 's/SPACESHIP_PROMPT_SEPARATE_LINE="${SPACESHIP_PROMPT_SEPARATE_LINE=true}"/SPACESHIP_PROMPT_SEPARATE_LINE="${SPACESHIP_PROMPT_SEPARATE_LINE=false}"/'
